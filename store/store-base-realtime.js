
'use strict';

import StoreBaseData from './store-base-data';
import GuidGenerator from 'z-abs-corelayer-cs/guid-generator';
import Action from '../communication/action';
import DataConfig from '../communication/data-config';
import MessageRealtimeError from 'z-abs-corelayer-cs/message/message-realtime/message-realtime-error';
import MessageRealtimeOpen from 'z-abs-corelayer-cs/message/message-realtime/message-realtime-open';
import MessageRealtimeClosed from 'z-abs-corelayer-cs/message/message-realtime/message-realtime-closed';


export default class StoreBaseRealtime extends StoreBaseData {
  constructor(state) {
    super(state);
    this.realtime = true;
    this.realtimeId = 0;
    this.realtimeListerners = new Map();
    this.workerRealtime = null;
    this.webSocket = null;
    this.cb;
    this.connected = false;
  }
  
  sendRealtimeAction(realtimeAction, sessionId) {
    if(0 !== this.realtimeListerners.size) {
      realtimeAction.setIds(GuidGenerator.create(), sessionId);
      this._sendRequest(realtimeAction.actionRequest, (response) => {
        realtimeAction.setResponses(response.responses);
        this.dispatchRealtimeAction(realtimeAction);
      });
    }
  }
  
  sendRealtimeMessage(msg, sessionId) {
    if(0 !== this.realtimeListerners.size) {
      this.workerRealtime.postMessage(['message', sessionId, msg]);
    }
  }
  
  handleRealtimeMessage(msg) {
    this.realtimeListerners.forEach((listener) => {
      listener.cbMessage(msg);
    });
    this.dispatching.on = true;
    this.dispatchRealtimeMessageAction(msg);
    this.dispatching.on = false;
    this._updateState();
  }
  
  addRealtimeListener(name, cbMessage) {
    if(0 === this.realtimeListerners.size) {
      this._initWebSocket();
    }
    this.realtimeListerners.set(++this.realtimeId, {
      name: name,
      cbMessage: cbMessage
    });
    const realtimeId = this.realtimeId;
    return {
      remove: () => {
        this.removeRealtimeListener(realtimeId);
      }
    }
  }
  
  removeRealtimeListener(realtimeId) {
    this.realtimeListerners.delete(realtimeId);
    if(0 === this.realtimeListerners.size) {
      this.workerRealtime.postMessage(['close']);
      this.workerRealtime.terminate();
    }
  }
  
  _worker() {
    let webSocket = null;
    onmessage = (e) => {
      if('message' === e.data[0]) {
        webSocket.send(JSON.stringify({
          type: 'DP_MSG',
          sessionId: e.data[1],
          data: e.data[2]
        }));
      }
      else if('request' === e.data[0]) {
        webSocket.send(JSON.stringify({
          type: 'DP_REQ',
          data: e.data[1]
        }));
      }
      else if('open' === e.data[0]) {
        webSocket = new WebSocket(e.data[1]);
        
        webSocket.onopen = () => {
          postMessage(['open']);
        };
        
        webSocket.onmessage = (msg) => {
          const message = JSON.parse(msg.data);
          if(undefined !== message.msgName) {
            postMessage(['message', message]);
          }
          else {
            postMessage(['response', message]);
          }
        };
        
        webSocket.onerror = (e) => {
          postMessage(['error']);
        };
        
        webSocket.onclose = (e) => {
          webSocket = null;
          postMessage(['close']);
        };
      }
      else if('close' === e.data[0]) {
        if(null !== webSocket) {
          webSocket.close(); 
          webSocket = null;
        }
      }
    }
  }
  
  _initWebSocket() {
    DataConfig.login(() => {
      const wsServer = DataConfig.wsServers.get('actorjs-ws-server');
      const workerToString = this._worker.toString();
      const start = workerToString.indexOf('{');
      const stop = workerToString.lastIndexOf('}');
      const workerString = workerToString.substring(start + 1, stop);
      this.workerRealtime = new Worker(URL.createObjectURL(new Blob([workerString], {type : 'text/javascript'})));
      this.workerRealtime.postMessage(['open', `ws://${wsServer.host}:${wsServer.port}`]);
      this.workerRealtime.onmessage = (e) => {
        switch(e.data[0]) {
          case 'open': {
            this.connected = true;
            this.handleRealtimeMessage(new MessageRealtimeOpen());
            break;
          }
          case 'message': {
            this.handleRealtimeMessage(e.data[1]);
            break;
          }
          case 'response' : {
            this.cb(e.data[1]);
            this.cb = undefined;
            break;
          }
          case 'error': {
            this.handleRealtimeMessage(new MessageRealtimeError());
            break;
          }
          case 'close': {
            this.connected = false;
            this.handleRealtimeMessage(new MessageRealtimeClosed());
            if(0 !== this.realtimeListerners.size) {
              this._initWebSocket();
            }
            break;
          }
        }
      };
    });
  }

  dispatchRealtimeAction(realtimeAction) {
    realtimeAction.actions.forEach((action) => {
      this.dispatching.action = action;
      this.dispatching.on = true;
      const handler = Reflect.get(this, `onRealtime${action.constructor.name}`);
      if(typeof handler === 'function') {
        if(undefined !== Reflect.apply(handler, this, [action])) {
          console.warn(`${this.constructor.name} store handler must not return values.`);
        }
      }
      else {
        console.warn(`REALTIME_ACTION: '${realtimeAction.constructor.name} - ACTION: ${action.constructor.name}(${this._logParams(action.params).join(', ')})' is not handled in the Store: '${this.constructor.name}'`);
      }
      this.dispatching.action = null;
      this.dispatching.on = false;
    });
    this._updateState();
  }
  
  dispatchRealtimeMessageAction(realtimeMessageAction) {
    const handler = Reflect.get(this, `onRealtime${realtimeMessageAction.msgName}`);
    if(typeof handler === 'function') {
      if(undefined !== Reflect.apply(handler, this, [realtimeMessageAction])) {
        console.warn(`${this.constructor.name} store handler must not return values.`);
      }
      if(this.state !== this.stateCurrent) {
        this.stateCurrent = this.state;
        this.listeners.forEach((listener, index) => {
          listener();
        });
      }
    }
  }
  
  _sendRequest(msg, cb) {
    this.workerRealtime.postMessage(['request', msg]);
    this.cb = cb;
  }
}
