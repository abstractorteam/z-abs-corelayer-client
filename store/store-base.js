
'use strict';

import Action from '../communication/action';
import StateUpdater from '../state/state-updater';
import Verbose from '../state/verbose';


export default class StoreBase {
  constructor(state) {
    this.state = state;
    this.stateCurrent = this.state;
    this.listeners = new Map();
    this.verbose = null;//new Verbose(false);
    this.stateUpdater = new StateUpdater(this.constructor.name, this.verbose);
    if(undefined !== this.onInit) {
      setTimeout(() => {
        this.onInit();
      });
    }
    this.dispatching = {
      action: null,
      on: false
    }
  }
  
  getState() {
    return this.state;
  }
  
  addListener(listener) {
    const id = ++StoreBase.listenerId;
    this.listeners.set(id, listener);
    return {
      remove: () => {
        this.listeners.delete(id);
      }
    };
  }
  
  updateState(execs) {
    if(!this.dispatching.on) {
      console.error('No update context');
      console.trace();
    }
    this.state = this.stateUpdater.updateState(execs, this.state, this.stateCurrent);
  }

  updateContext(cb) {
    this.dispatching.on = true;
    cb();
    this.dispatching.on = false;
    this._updateState();
  }
  
  shallowCopy(object) {
    return this.stateUpdater.shallowCopy(object);
  }
  
  dispatchAction(action) {
    const handler = Reflect.get(this, `on${action.constructor.name}`);
    if(typeof handler === 'function') {
      this.dispatching.action = action;
      this.dispatching.on = true;
      if(undefined !== Reflect.apply(handler, this, [action])) {
        console.warn(`${this.constructor.name} store handler must not return values.`);
      }
      this.dispatching.action = null;
      this.dispatching.on = false;
      this._updateState();
    }
    else {
      console.warn(`ACTION: '${action.constructor.name}(${this._logParams(action.params).join(', ')})' is not handled in the Store: '${this.constructor.name}'`);
    }
  }
  
  _updateState() {
    if(this.state !== this.stateCurrent) {
      this.stateCurrent = this.state;
      this.listeners.forEach((listener, index) => {
        listener();
      });
    }
  }
  
  _logParams(params) {
    if(undefined === params) {
      return [];
    }
    return params.map((param, index) => {
      if(param && typeof param === 'string') {
        return `'${param}'`;
      }
      else if(Array.isArray(param)) {
        return `[${this._logParams(param).join(', ')}]`;
      }
      else {
        return param;
      }
    });
  }
}

StoreBase.listenerId = 0;

StoreBase.SUCCESS = 'success';
StoreBase.FAILURE = 'failure';
StoreBase.ERROR = 'error';

StoreBase.WAIT_ONCE = 0;
StoreBase.WAIT_ON = 1;
