
'use strict';

import { DataActionDialogFileGet, DataActionDialogFileVerifyOrCreate } from '../actions/action-dialog-file';
import StoreBaseData from 'z-abs-corelayer-client/store/store-base-data';
import Project from 'z-abs-corelayer-cs/project';
import Path from 'path' ;


class DialogFileStore extends StoreBaseData {
  constructor() {
    super({
      project: new Project(),
      current: {
        file: null,
        folder: null,
        type: ''
      },
      original: {
        file: null,
        folder: null,
        type: ''
      }
    });
  }

  onInit() {
    this.sendDataAction(new DataActionDialogFileVerifyOrCreate());
  }

  onActionDialogFileSet(action) {
    let node = this.state.project.findNode(action.path);
    if(undefined !== node) {
      if(node.data.path.startsWith(`${this.state.original.folder.data.path}/${this.state.original.folder.title}`)) {
        this.updateState({current: {type: {$set: node.folder ? 'folder' : 'file'}}});
        if(node.folder) {
          this.updateState({current: {folder: {$set: node}}});
        }
        else {
          this.updateState({current: {file: {$set: node}}});
        }
      }
    }
  }
  
  onActionDialogFileGet(action) {
    this.updateState({project: {$set: new Project()}});
    this.updateState({current: {$set: {
      file: null,
      folder: null,
      type: ''
    }}});
    this.updateState({original: {$set: {
      file: null,
      folder: null,
      type: ''
    }}});
    this.sendDataAction(new DataActionDialogFileGet(action.path, action.rootName, action.pathFilters, action.existingFilters, action.type));
  }
  
  onDataActionDialogFileVerifyOrCreate(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
    
    }
  }
  
  onDataActionDialogFileGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({project: (project) => { project.set(response.data.source); }});
      let node = this.state.project.findNode(action.path);
      if(undefined !== node) {
        this.updateState({current: {file: {$set: null}}});
        this.updateState({current: {folder: {$set: node}}});
        this.updateState({current: {type: {$set: 'folder'}}});
        this.updateState({original: {file: {$set: null}}});
        this.updateState({original: {folder: {$set: node}}});
        this.updateState({original: {type: {$set: 'folder'}}});
      }
    }
  }
}

module.exports = new DialogFileStore();
