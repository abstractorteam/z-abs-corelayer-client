

class Verbose {
  constructor(details) {
    this.details = details;
    this.compareId = 0;
    this.compareResult = '';
  }
  
  getDetails() {
    return this.details;
  }
  
  getCompareId() {
    return this.compareId;
  }
  
  clear() {
    this.compareId = 0;
    this.compareResult = '';
  }
  
  compare() {
    ++this.compareId;
  }
}

module.exports = Verbose;
