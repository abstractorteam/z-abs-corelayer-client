
'use strict';


export default class StateUpdater {
  constructor(ownerName, verbose) {
    this.ownerName = ownerName;
    this.verbose = verbose;
  }
  
  shallowCompare(thisCompare, nextCompare) {
    if(this.verbose) {
       this.verbose.compare();
    }
    return this._shallowCompare(thisCompare, nextCompare, 0, (message) => {
      this.verbose.compareResult = message;
    });
  }
  
  shallowCompareObjectValues(thisCompare, nextCompare) {
    if(this.verbose) {
       
    }
    return this._shallowCompareObjectValues(thisCompare, nextCompare, 0, (message) => {
      this.verbose.compareResult = message;
    });
  }
  
  shallowCompareArrayValues(thisCompare, nextCompare) {
    if(this.verbose) {
       
    }
    return this._shallowCompareArrayValues(thisCompare, nextCompare, 0, (message) => {
      this.verbose.compareResult = message;
    });
  }
  
  deepCompare(thisCompare, nextCompare) {
    if(this.verbose) {
       
    }
    return this._deepCompare(thisCompare, nextCompare, 0, (message) => {
      this.verbose.compareResult = message;
    });
  }
  
  _shallowCompare(thisCompare, nextCompare, deepth, cb) {
    if(thisCompare !== nextCompare) {
      if(this.verbose) {
        const details = this.verbose.getDetails() ? `${JSON.stringify(thisCompare, null, 2)} !== ${JSON.stringify(nextCompare, null, 2)}` : '';
        cb(`${'-- '.repeat(deepth)}${this.ownerName}.shallowCompare[${typeof thisCompare}, ${typeof nextCompare}]: ${details}`);
      }
      return false;
    }
    return true;
  }
  
  _shallowCompareObjectValues(thisCompare, nextCompare, deepth, cb) {
    if(undefined !== thisCompare && undefined !== nextCompare) {
      let thisKeys = Reflect.ownKeys(thisCompare);
      let nextKeys = Reflect.ownKeys(nextCompare);
      if(!this._shallowCompareArrayValues(thisKeys, nextKeys, deepth + 1, (msg) => {
      })) {
        return false;
      }
      return thisKeys.every((key, index) => {
        let thisReflect = Reflect.get(thisCompare, thisKeys[index]);
        let nextReflect = Reflect.get(nextCompare, thisKeys[index]);
        if(Array.isArray(thisReflect) && Array.isArray(nextReflect)) {
          return this._shallowCompareArrayValues(thisReflect, nextReflect, deepth + 1, (msg) => {
            cb(`${'-- '.repeat(deepth)}${this.ownerName}.shallowCompareObjectValues: \n${msg}`);
          });
        }
        else {
          return this._shallowCompare(thisReflect, nextReflect, deepth + 1, (msg) => {
            cb(`${'-- '.repeat(deepth)}${this.ownerName}.shallowCompareObjectValues: \n${msg}`);
          });
        }
      });
    }
    else {
      return this._shallowCompare(thisCompare, nextCompare, deepth + 1, (msg) => {
        cb(`${'-- '.repeat(deepth)}${this.ownerName}.shallowCompareObjectValues: \n${msg}`);
      });
    }
  }
  
  _shallowCompareArrayValues(thisCompares, nextCompares, deepth, cb) {
    if(undefined !== thisCompares && undefined !== nextCompares && thisCompares.length === nextCompares.length) {
      return thisCompares.every((thisCompare, index) => {
        return this._shallowCompare(thisCompare, nextCompares[index], deepth + 1, (msg) => {
          cb(`${'-- '.repeat(deepth)}${this.ownerName}.shallowCompareArrayValues: \n${msg}`);
        });
      });
    }
    else {
      return this._shallowCompare(thisCompares, nextCompares, deepth + 1, (msg) => {
         cb(`${'-- '.repeat(deepth)}${this.ownerName}.shallowCompareArrayValues: \n${msg}`);
      });
    }
  }
  
  _deepCompare(thisCompare, nextCompare, deepth, cb) {
    let thisCompareType = typeof thisCompare;
    let nextCompareType = typeof nextCompare;
    if(thisCompareType !== nextCompareType) {
      if(this.verbose) {
        let details = this.verbose.getDetails() ? `${JSON.stringify(thisCompare, null, 2)} !== ${JSON.stringify(nextCompare, null, 2)}` : '';
        cb(`${'-- '.repeat(deepth)}${this.ownerName}.deepCompare: typeof - ${thisCompareType} !== ${nextCompareType}: ${details}`);
      }
      return false;
    }
    if('object' === thisCompareType) {
      if(Array.isArray(thisCompare)) {
        if(Array.isArray(nextCompare)) {
          if(thisCompare.length !== nextCompare.length) {
            if(this.verbose) {
              let details = this.verbose.getDetails() ? `${JSON.stringify(thisCompare, null, 2)} !== ${JSON.stringify(nextCompare, null, 2)}` : '';
              cb(`${'-- '.repeat(deepth)}${this.ownerName}.deepCompare[Array]length !== length: ${details}`);
            }
            return false;
          }
          return thisCompare.every((thisCompare, index) => {
            return this.deepCompare(thisCompare, nextCompare[index], deepth + 1, (msg) => {
              cb(`${'-- '.repeat(deepth)}${this.ownerName}.deepCompare[Array] : \n${msg}`);
            });
          });
        }
        else {
          if(this.verbose) {
            let details = this.verbose.getDetails() ? `${JSON.stringify(thisCompare, null, 2)} !== ${JSON.stringify(nextCompare, null, 2)}` : '';
            cb(`${'-- '.repeat(deepth)}${this.ownerName}.deepCompare[Array]: Array !== !Array: ${details}`);
          }
          return false;
        }
      }
      else {
        if(undefined === thisCompare) {
          if(undefined !== nextCompare) {
            let details = this.verbose.getDetails() ? `${thisCompare} !== ${nextCompare}` : '';
            cb(`${'-- '.repeat(deepth)}${this.ownerName}.deepCompare[undefined, object]: ${details}`);
            return false;
          }
          else {
            return true;
          }
        }
        else if(null === thisCompare) {
          if(null !== nextCompare) {
            let details = this.verbose.getDetails() ? `${thisCompare} !== ${nextCompare}` : '';
            cb(`${'-- '.repeat(deepth)}${this.ownerName}.deepCompare[null, object]: ${details}`);
            return false;
          }
          else {
            return true;
          }
        }
        let thisKeys = Reflect.ownKeys(thisCompare);
        let nextKeys = Reflect.ownKeys(nextCompare);
        if(!this.shallowCompareArrayValues(thisKeys, nextKeys, deepth + 1, (msg) => {
          cb(`${'-- '.repeat(deepth)}${this.ownerName}.deepCompare[object, object]: \n${msg}`);
        })) {
          return false;
        }
        return thisKeys.every((key, index) => {
          return this.deepCompare(Reflect.get(thisCompare, thisKeys[index]), Reflect.get(nextCompare, thisKeys[index]), deepth + 1, (msg) => {
            cb(`${'-- '.repeat(deepth)}${this.ownerName}.deepCompare[object, object]: \n${msg}`);
          });
        });
      }
    }
    else {
  //  if('string' === thisCompareType || 'number' === thisCompareType || 'boolean' === thisCompareType) {
      return this._shallowCompare(thisCompare, nextCompare, deepth + 1, (msg) => {
        cb(`${'-- '.repeat(deepth)}${this.ownerName}.deepCompare[${thisCompareType}]: \n${msg}`);
      });
    }
    /*else {
      if(this.verbose) {
        let details = this.verbose.getDetails() ? `${JSON.stringify(thisCompare, null, 2)}  AND ${JSON.stringify(nextCompare, null, 2)}` : '';
        cb(`${'-- '.repeat(deepth)}${this.ownerName}.deepCompare: NOT IPLMEMENTED: ${details}`);
      }
      return false;
    }*/
  }
  
  updateState(execs, state, stateCurrent) {
    return this._updateState(execs, state, stateCurrent);
  }
  
  shallowCopy(object) {
    if(Array.isArray(object)) {
      let a = [];
      for(let i = 0; i < object.length; ++i) {
        a[i] = object[i];
      }
      return a;
    }
    else if(object && typeof object === 'object') {
      if(object instanceof Map) {
        return new Map(object);
      }
      else if(object instanceof Set) {
        return new Set(object);
      }
      else {
        let s = object.toString();
        if('[object Map]' === s) {
          return new Map(object);
        }
        else if('[object Set]' === s) {
          return new Set(object);
        }
        return Object.assign(new object.constructor(), object);
      }
    }
    else {
      return object;
    }
  }

  _updateState(execs, state, stateCurrent) {
    let stateNext = state;
    if(typeof execs === 'function') {
      if(stateNext === stateCurrent) {
        stateNext = this.shallowCopy(stateCurrent);
      }
      let updated = false;
      execs(new Proxy(stateNext, {
        get(target, propKey, receiver) {
          if(typeof target[propKey] !== 'function') {
            return target[propKey];
          }
          else {
            const origMethod = target[propKey];
            if(target instanceof Map || '[object Map]' === target.toString()) {
              return function (...args) {
                if('set' === propKey) {
                  let current = target['get'].apply(target, [args[0]]);
                  if(current !== args [1]) {
                    updated = true;
                    return origMethod.apply(target, args);
                  }
                }
                else if('clear' === propKey || 'delete' === propKey) {
                  updated = true;
                  return origMethod.apply(target, args);
                }
                else {
                  return origMethod.apply(target, args);
                }
              };
            }
            else {
              return function (...args) {
                updated = true;
                let result = origMethod.apply(target, args);
                return result;
              };
            }
          }
        },
        set(obj, prop, value) {
          updated = true;
          return Reflect.set(obj, prop, value);
        }
      }));
      if(updated) {
        return stateNext;
      }
      else {
        return state;
      }
    }
    else {
      const keys = Reflect.ownKeys(execs);
      if(0 !== keys.length && '$set' === keys[0]) {
        return Reflect.get(execs, '$set');
      }
      else {
        for(let i = 0; i < keys.length; ++i) {
          const stateChild = this._updateState(Reflect.get(execs, keys[i]), Reflect.get(state, keys[i]), Reflect.get(stateCurrent, keys[i]));
          const current = Reflect.get(stateCurrent, keys[i]);
          if(stateChild !== current) {
            if(stateNext === stateCurrent) {
              stateNext = this.shallowCopy(stateCurrent);
            }
            Reflect.set(stateNext, keys[i], stateChild);
          }
        }
      }
    }
    return stateNext;
  }  
}
