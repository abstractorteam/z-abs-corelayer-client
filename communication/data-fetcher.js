
'use strict';

import DataAction from './data-action';
import GuidGenerator from 'z-abs-corelayer-cs/guid-generator';


class DataFetcher {
  constructor() {
    const workerToString = this._worker.toString();
    const start = workerToString.indexOf('{');
    const stop = workerToString.lastIndexOf('}');
    const workerString = workerToString.substring(start + 1, stop);
    this.requests = new Map();
    this.workerRealtime = new Worker(URL.createObjectURL(new Blob([workerString], {type : 'text/javascript'})));
    this.workerRealtime.onmessage = (e) => {
      const guid = e.data[0];
      const responses = e.data[1];
      if(null !== responses) {
        const cbData = this.requests.get(guid);
        if(cbData) {
          cbData.dataAction.setResponses(responses);
          cbData.store.dispatchDataAction(cbData.dataAction);
          cbData.cb();
        }
      }
      this.requests.delete(guid);
    };
  }
  
  send(dataAction, store, cb) {
    const guid = GuidGenerator.create();
    dataAction.setIds(guid);
    this.requests.set(guid, {
      dataAction: dataAction,
      store: store,
      cb: cb
    });
    this.workerRealtime.postMessage(dataAction.actionRequest);
  }
    
  _worker() {
    let webSocket = null;
    onmessage = (e) => {
      const actionRequest = e.data;
      fetch(`${self.location.origin}/data/`, {
        method: 'post',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(actionRequest, (key, value) => {
          if(key.startsWith('_')) {
            return undefined;
          }
          return value;
        })
      }).then((response) => response.json()
      ).then((data) => {
        if(undefined !== data) {
          postMessage([actionRequest.id, data.responses]);
        }
        else {
          postMessage([actionRequest.id, null]);
        }
      }).catch((e) => {
        postMessage([actionRequest.id, null]);
      });
    };
  }
}

DataFetcher.self = new DataFetcher();


module.exports = DataFetcher.self;
