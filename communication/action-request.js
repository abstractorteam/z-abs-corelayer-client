
'use strict';

export default class ActionRequest {
  constructor(requests = []) {
    this.id = '';
    this.requests = requests;
  }
  
  setIds(requestId, sessionId) {
    this.id = requestId;
    this.requests.forEach((request) => {
      request.sessionId = sessionId;
    });
  }
  
  add(name, index, ...params) {
    this.requests.push({
      name: name,
      index: index,
      sessionId: undefined,
      params: [...params]
    });
    return this;
  }
}
