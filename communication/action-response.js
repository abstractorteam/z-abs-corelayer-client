
'use strict';

export default class ActionResponse {
  constructor(result, data) {
    this.result = result;
    this.data = data;
  }
  
  isSuccess() {
    return 'success' === this.result.code;
  }
  
  static parse(state, responses) {
    responses.forEach((value) => {
      if(state.name !== value.name) {
        return;
      }
      state.result = value.result;
      if('success' === value.result.code) {
        state.data = Reflect.get(value.data, state.name);
      }
    });
  }
}
