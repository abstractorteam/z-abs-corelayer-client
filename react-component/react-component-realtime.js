
'use strict';

import React from 'react';
import ReactComponentStore from './react-component-store';


export default class ReactComponentRealtime extends ReactComponentStore {
  constructor(props, stores, state) {
    super(props, stores, state);
    this.realtimeFrameCurrentNames = new Map();
    this.realtimeCallbacks = [];
    this.realtimeCallbacksNext = [];
    this.realtimeFrameReplaceQueue = [];
    this.realtimeRemovers = [];
    this.cbIsRealtime = () => { return false; }
    if(undefined === stores || 0 === stores.length) {
      console.warn(`In ${this.constructor.name} extend ReactComponentBase, when there are no stores, instead of ReactComponentRealtime.`);
    }
    else {
      if(!stores.some((store) => {
        return store.realtime;
      })) {
        console.warn(`In ${this.constructor.name} extend ReactComponentStore, when there are no stores supporting realtime, instead of ReactComponentRealtime.`);
      }
    }
    this.updating = false;
  }

  componentDidMount() {
    super.componentDidMount();
    this.stores.forEach((store) => {
      if(typeof store.addRealtimeListener === 'function') {
        this.realtimeRemovers.push(store.addRealtimeListener(this.constructor.name, (message) => {
          this.onRealtimeMessage(message);
        }));
      }
    });
  }
  
  componentWillUnmount() {
    this.realtimeRemovers.forEach((realtimeRemover) => {
      realtimeRemover.remove();
    });
    super.componentWillUnmount();
  }
  
  shouldComponentUpdate(nextProps, nextState) {
    if(!this.cbIsRealtime()) {
      return super.shouldComponentUpdate(nextProps, nextState);
    }
    else {
      if(this.realtimeUpdate) {
        this.realtimeUpdate(nextProps, nextState);
      }
      return false;
    }
  }
  
  onRealtimeMessage(msg) {}
  
  addRealtimeCallback(callback, nextCallback) {
    this.realtimeCallbacks.push(callback);
    nextCallback && this.realtimeCallbacksNext.push(nextCallback);
    this.renderRealtimeFrame();
  }
  
  addRealtimeFrameCurrent(name, value) {
    this.realtimeFrameCurrentNames.set(name, value);
  }
  
  clearRealtimeFrames() {
    this.realtimeFrameCurrentNames = new Map();
    this.realtimeFrameReplaceQueue = [];
  }
  
  addRealtimeFrameReplaceQueueText(name, nextText) {
    this.realtimeFrameReplaceQueue.push({
      name: name,
      next: document.createTextNode(nextText)
    });
  }

  addRealtimeFrameReplaceQueueObject(name, nextObject) {
    this.realtimeFrameReplaceQueue.push({
      name: name,
      next: nextObject
    });
  }
  
  renderRealtimeFrame(clear=false) {
    if(!this.updating) {
      this.updating = true;
      requestAnimationFrame((timestamp) => {
        const t1 = performance.now();
        while(performance.now() - t1 < 24.0) {
          const cb = this.realtimeCallbacks.shift();
          if(undefined === cb) {
            break;
          }
          cb();
        }
        this.onRenderRealtimeFrame(timestamp, clear);
        this.realtimeFrameReplaceQueue.forEach((value) => {
          const current = this.realtimeFrameCurrentNames.get(value.name);
          current.parentNode.replaceChild(value.next, current);
          this.realtimeFrameCurrentNames.set(value.name, value.next);
        });
        this.realtimeFrameReplaceQueue = [];
        if(0 !== this.realtimeCallbacksNext.length) {
          console.log('BEFORE', this.realtimeCallbacks.length, this.realtimeCallbacksNext.length);
          this.realtimeCallbacks.push.apply(this.realtimeCallbacks, this.realtimeCallbacksNext);
          this.realtimeCallbacksNext = []; 
        }
        this.updating = false;
        if(0 !== this.realtimeCallbacks.length) {
          this.renderRealtimeFrame();
        }
      });
    }
  }
}
