
'use strict';


class Context {
  constructor() {
    this.contexts = new Map();
  }
  
  set(name, context, reactComponent) {
    this.contexts.set(name, {
      context,
      reactComponent
    });
  }
  
  get(name) {
    const contextData = this.contexts.get(name);
    if(contextData) {
      return contextData.context;
    }
  }
}


module.exports = new Context();
