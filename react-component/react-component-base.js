
'use strict';

import StateUpdater from '../state/state-updater';
import Verbose from '../state/verbose';
import Context from './context';
import React from 'react';


export default class ReactComponentBase extends React.Component {
  constructor(props, state={}) {
    super(props);
    this.state = state;
    this.stateNext = state;
    this.myContext = new Proxy(Context, {
      get(target, property, receiver) {
        return target.get(property);
      },
      set(target, property, value, receiver) {
        target.set(property, value, this);
        return true;
      }
    });
    this.titleFunc = null;
    this.verbose = null;//new Verbose(true); //null
    this.stateUpdater = new StateUpdater(this.constructor.name, this.verbose);
  }
  
  componentDidMount() {
    this.stateNext = this.state;
    try {
      this.didMount && this.didMount();
      if(this.verbose) {
        console.log(`${++ReactComponentBase.logId}: didMount: ${this.constructor.name}`);
      }
    }
    catch(err) {
      this._logError(`didMount: ${this.constructor.name}`, err);
    }
    this._setTitle();
  }
  
  
  UNSAFE_componentWillReceiveProps(nextProps) {
    try {
      this.willReceiveProps && this.willReceiveProps(nextProps);
    }
    catch(err) {
      this._logError('willReceiveProps', err);
      return true;
    }    
  }
  
  shouldComponentUpdate(nextProps, nextState) {
    if(!this.shouldUpdate) {
      console.warn(`${this.constructor.name}.shouldUpdate is not implemented.`);
      return true;
    }
    try {
      if(!this.verbose) {
        return this.shouldUpdate(nextProps, nextState);
      }
      else {
        this.verbose.clear();
        if(this.shouldUpdate(nextProps, nextState)) {
          console.log(`${++ReactComponentBase.shouldUpdateId}: ${this.constructor.name}.shouldUpdate: ${this.verbose.compareResult}`);
          return true;
        }
        else {
        //  if(this.props._uriPath && !this.shallowCompare(this.props._uriPath, nextProps._uriPath)) {
          //  console.log(`${++ReactComponentBase.shouldUpdateId}: ${this.constructor.name}.shouldUpdate: ${this.verbose.compareResult}`);
        //    return true;
      //    }
    //      else {
            return false;
       //   }
        }
      }
    }
    catch(err) {
      this._logError('shouldUpdate', err);
      return true;
    }
  }
  
  UNSAFE_componentWillUpdate(nextProps, nextState) {
    try {
      this.willUpdate && this.willUpdate(nextProps, nextState);
    }
    catch(err) {
      this._logError('willUpdate', err);
    }
  }
  
  componentDidUpdate(prevProps, prevState, snapshot) {
    this.stateNext = this.state;
    try {
      this.didUpdate && this.didUpdate(prevProps, prevState, snapshot);
    }
    catch(err) {
      this._logError('didUpdate', err);
    }
    if(this.verbose) {
      console.log(`${++ReactComponentBase.logId}: didUpdate: ${this.constructor.name}`);
    }
    this._setTitle();
  }
  
  componentWillUnmount() {
    try {
      this.willUnmount && this.willUnmount();
    }
    catch(err) {
      this._logError('willUnmount', err);
    }
  }
  
  shallowCompare(thisCompare, nextCompare) {
    return this.stateUpdater.shallowCompare(thisCompare, nextCompare);
  }
  
  shallowCompareObjectValues(thisCompare, nextCompare) {
    return this.stateUpdater.shallowCompareObjectValues(thisCompare, nextCompare);
  }
  
  shallowCompareArrayValues(thisCompare, nextCompare) {
    return this.stateUpdater.shallowCompareArrayValues(thisCompare, nextCompare);
  }
  
  deepCompare(thisCompare, nextCompare) {
    return this.stateUpdater.deepCompare(thisCompare, nextCompare);
  }
  
  setState(nextState, callback) {
    console.warn(this.constructor.name + '.setState is forbidden. Use updateState instead.');
    super.setState(nextState, callback);
  }
  
  updateState(execs) {
    this.stateNext = this.stateUpdater.updateState(execs, this.stateNext, this.state);
    super.setState(this.stateNext);
  }
  
  shallowCopy(object) {
    return this.stateUpdater.shallowCopy(object);
  }
  
  _logError(member, err) {
    console.error(`Exception in ${this.constructor.name}.${member}. Message: '${err.message}'`);
    if(undefined !== err.stack) {
      console.error(`Stack: '${err.stack}'`);
    }
  }
  
  setTitle(titleFunc) {
    this.titleFunc = titleFunc;
  }
  
  _setTitle() {
    if(this.titleFunc) {
      const title = this.titleFunc();
      if(title.isPath) {
        const parts = title.text.split('/');
        document.title = `${title.prefix ? title.prefix : ''}${parts[parts.length - 1]}`;
      }
      else {
        document.title = `${title.prefix ? title.prefix : 'ActorJs'} - ${title.text}`;
      }
    } 
  }
}

ReactComponentBase.TitleSize = 16;
ReactComponentBase.popoverId = 0;
ReactComponentBase.logId = 0;
ReactComponentBase.shouldUpdateId = 0;
