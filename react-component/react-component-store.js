
'use strict';

import React from 'react';
import ReactComponentBase from './react-component-base';


export default class ReactComponentStore extends ReactComponentBase {
  constructor(props, stores=[], state={}) {
    super(props, state);
    this.stores = stores;
    this.removers = [];
    this.stores.forEach((store) => {
      Reflect.set(this.state, store.constructor.name, store.getState());
    });
  }

  componentDidMount() {
    super.componentDidMount();
    this.stores.forEach((store) => {
      const onStoreFunction = Reflect.get(this, `on${store.constructor.name}`);
      if(typeof onStoreFunction === 'function') {
        const onStoreFunctionBound = onStoreFunction.bind(this);
        this.removers.push(store.addListener(() => {
          Reflect.set(this.state, store.constructor.name, store.getState());
          this.setState(this.state, () => {
            onStoreFunctionBound();
          });
        }));
      }
      else {
        this.removers.push(store.addListener(() => {
          const updatedState = {};
          Reflect.set(updatedState, store.constructor.name, {$set: store.getState()});
          this.updateState(updatedState);
        }));
      }
    });
  }
  
  componentWillUnmount() {
    this.removers.forEach((remover) => {
      remover.remove();
    });
    super.componentWillUnmount();
  }
  
  dispatch(store, action) {
    if(this.context.attachHistory) {
      this.context.attachHistory(action);
    }
    store.dispatchAction(action);
  }
}
