
'use strict';

import Action from '../communication/action';
import DataAction from '../communication/data-action';


export class ActionDialogFileSet extends Action {
  constructor(path) {
    super(path);
  }
}

export class ActionDialogFileGet extends Action {
  constructor(path, rootName, pathFilters, existingFilters, type) {
    super(path, rootName, pathFilters, existingFilters, type);
  }
}

export class DataActionDialogFileGet extends DataAction {
  constructor(path, rootName, pathFilters, existingFilters, type) {
    super();
    this.addRequest(new ActionDialogFileGet(path), path, rootName, pathFilters, existingFilters, type);
  }
}

export class ActionDialogFileVerifyOrCreate extends Action {
  constructor() {
    super();
  }
}

export class DataActionDialogFileVerifyOrCreate extends DataAction {
  constructor() {
    super();
    this.addRequest(new ActionDialogFileVerifyOrCreate());
  }
}
