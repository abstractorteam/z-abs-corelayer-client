
'use strict';

import ReactComponentBase from 'z-abs-corelayer-client/react-component/react-component-base';
import React from 'react';


export default class NotFound extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.setTitle(() => {
      return {
        isPath: this.props._,
        text: this.props._ ? this.props._[0] : 'Not Found',
        prefix: 'NF: '
      };
    });
  }
  
  shouldUpdate(nextProps, nextState) {
    return true
  }
  
  render() {
    return (
      <div className="not_found_page">
        <img id="not_found_page" src="/images/svg/AbstraktorLogo.svg"></img>
        <h1 className="not_found_page">
          Page Not Found
        </h1>
        <br />
        <h4>
          Links to Stacks, Actors, ConnectionWorkers, Test Cases and Test Suites are available from the ActorJs tool.
        </h4>
      </div>
    );
  }
}
